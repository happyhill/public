
/* ------------------------------------------------------------------------------------------------
	ヘッダ
------------------------------------------------------------------------------------------------ */
#include <iostream>
#include <functional>
#include <unordered_map>
#include <algorithm>

#include <string>


/* ------------------------------------------------------------------------------------------------
	デバッグ出力
------------------------------------------------------------------------------------------------ */

#if defined(_DEBUG)
	#define debugPrint(Str) std::cout << Str;
#else
	#define debugPrint(A)
#endif


/* ------------------------------------------------------------------------------------------------
	マネージャテンプレートクラス
------------------------------------------------------------------------------------------------ */
template<typename T>
class GenericManager final
{


	//	別名
public:
	using Pair = std::pair<uint32_t, T*>;


	//	コンストラクタ / デストラクタ
protected:
	inline GenericManager() {}


	//	関数
public:
	inline static GenericManager<T>& instance()			{ static GenericManager<T> instance; return instance; }
	inline void registration(uint32_t key, T* instance) { mRegistratedIndex.emplace(key, instance); }
	inline void registration(T* instance);
	inline void erace(T* instance);

	template <class Function>
	inline void forEach(Function function);


	//	変数
private:
	std::unordered_map<uint32_t, T*> mRegistratedIndex;


};

//===============================================
//	関数
//===============================================
template<typename T>
inline void GenericManager<T>::registration(T* instance)
{
	for (uint32_t key = 0;; ++key)
	{
		//	未使用keyが見つかったら
		if (mRegistratedIndex.count(key) == 0)
		{
			mRegistratedIndex.insert(std::pair<uint32_t, T*>{ key, instance });
			return;
		}
	}

	debugPrint("registration error.(" << std::hex << instance << ")\n");
}

template<typename T>
inline void GenericManager<T>::erace(T* instance)
{
	for (auto& itr : mRegistratedIndex)
	{
		if (itr.second == instance)
		{
			mRegistratedIndex.erase(itr.first);

			//debugPrint("erace success.(" << std::hex << instance << ")\n");
			return;
		}
	}

	debugPrint("erace error.(" << std::hex << instance << ")\n");
}

//	デフォルト実行
template<typename T>
template<class Function>
inline void GenericManager<T>::forEach(Function function)
{
	std::for_each(mRegistratedIndex.begin(), mRegistratedIndex.end(), function);
}


/* ------------------------------------------------------------------------------------------------
	インスタンスクラス
------------------------------------------------------------------------------------------------ */
class Instance final
{


	//	別名
public:
	using Manager = GenericManager<Instance>;


	//	コンストラクタ / デストラクタ
public:
	explicit Instance(uint32_t value) : mValue(value)				{ Manager::instance().registration(this);	debugPrint("New Instance has created. (" << std::hex << this << ")\n"); }
	Instance(Instance&& instance)	  : mValue(instance.getValue())	{ Manager::instance().registration(this);	debugPrint("Instance has moved.       (" << std::hex << &instance << "->" << std::hex << this << ")\n"); }
	~Instance()														{ Manager::instance().erace(this);			debugPrint("Instance has deleted.     (" << std::hex << this << ")\n"); }


	//	関数
public:
	void		setValue(uint32_t value) { mValue = value; }
	uint32_t	getValue()		const { return mValue; }
	void		printValue()	const { std::cout << std::dec << mValue << "\n"; }
	void		printValueHex()	const { std::cout << std::hex << mValue << "\n"; }


	//	Non-copyable処理
public:
	explicit Instance(const Instance&)			 = delete;
	Instance& operator =(const Instance& source) = delete;


	//	変数
private:
	uint32_t mValue;


};


/* ------------------------------------------------------------------------------------------------
	マネージャクラスの特殊化
------------------------------------------------------------------------------------------------ */


/* ------------------------------------------------------------------------------------------------
	エントリーポイント
------------------------------------------------------------------------------------------------ */
int main()
{
	//	ローカル変数の場合
	{
		Instance instance(21);
		Instance::Manager::instance().forEach([](auto pair) { pair.second->printValue(); });
	}

	std::cout << "------------------------------------------------------------------------\n";

	//	コンテナにブチ込む場合
	{
		std::vector<Instance> instances;
		instances.push_back(Instance(2));
		instances.push_back(Instance(3));
		instances.push_back(Instance(5));
		instances.push_back(Instance(7));
		instances.push_back(Instance(11));
		instances.push_back(Instance(13));
		Instance::Manager::instance().forEach([](auto pair) { pair.second->printValue();			});
		std::cout << "<適当に数値をDEADC0DEにしつつHEX表示>\n";
		Instance::Manager::instance().forEach([](auto pair) { pair.second->setValue(0xDEADC0DE);	});
		Instance::Manager::instance().forEach([](auto pair) { pair.second->printValueHex();			});
		std::cout << "<適当に3つ消す>\n";
		instances.pop_back();
		instances.pop_back();
		instances.pop_back();
		Instance::Manager::instance().forEach([](auto pair) { pair.second->printValueHex();			});
	}

	std::cout << "------------------------------------------------------------------------\n";

	//	スマポにブチ込む場合(shared)
	{
		auto instance = std::make_shared<Instance>(73);
		Instance::Manager::instance().forEach([](auto pair) { pair.second->printValue(); });
	}

	//	スマポにブチ込む場合(unique)
	{
		auto instance = std::make_unique<Instance>(52);
		Instance::Manager::instance().forEach([](auto pair) { pair.second->printValue(); });
	}


	//	おわり
	getchar();

	return 0;
}