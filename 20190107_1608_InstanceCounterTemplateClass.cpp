/* ==================================================================================================================================================
	
	Instance Counter Template Class (Original)
		
		remark:	特定のインスタンスに対して、自動的に生成されている数をカウントする機能を提供するためのクラステンプレート

================================================================================================================================================== */
/* ------------------------------------------------------------------------------------------------
	ヘッダ
------------------------------------------------------------------------------------------------ */
#include <typeinfo>
#include <iostream>
#include <functional>
#include <unordered_map>
#include <algorithm>

#include <memory>
#include <string>


/* ------------------------------------------------------------------------------------------------
	debug名前空間
------------------------------------------------------------------------------------------------ */
namespace debug
{


	/* ------------------------------------------------------------------------------------------------
		定数
	------------------------------------------------------------------------------------------------ */
	constexpr uint32_t DEBUG_MESSAGE_ADDRESS_COLUMN = 95;

	enum MessageType : uint8_t
	{
		DefaultConstructor	= 0,
		CopyConstructor		= 1,
		DefaultDestructor	= 2,
	};


	/* ------------------------------------------------------------------------------------------------
		ユーティリティ関数
	------------------------------------------------------------------------------------------------ */
	// ----------------------------------------------
	//	コンストラクタ及びデストラクタで、それらが呼び出された事をコンソールに通知する
	// ----------------------------------------------
	void debugMessageForConstructionAndDestruction(const std::string& classname, void* address, MessageType messageType)
	{
		std::string str;
		switch (messageType)
		{
			case DefaultConstructor:
				str = "[DEBUG]: New " + classname + " has constructed.";
				break;
			case CopyConstructor:
				str = "[DEBUG]: New " + classname + " has constructed, from Copy constructor.";
				break;
			case DefaultDestructor:
				str = "[DEBUG]: " + classname + " has destructed.";
				break;
			default:
				break;
		}

		for (; str.length() < DEBUG_MESSAGE_ADDRESS_COLUMN;)
		{
			str += " ";
		}

		std::cout << str << ": 0x" << std::hex << address << "\n";
	}


	/* ------------------------------------------------------------------------------------------------
		ヘルパマクロ
	------------------------------------------------------------------------------------------------ */
	#if defined(_DEBUG)
	
		#define DEBUG_CONSTRUCT_MESSAGE			debug::debugMessageForConstructionAndDestruction(typeid(*this).name(), reinterpret_cast<void*>(this), debug::MessageType::DefaultConstructor)
		#define DEBUG_COPY_CONSTRUCT_MESSAGE	debug::debugMessageForConstructionAndDestruction(typeid(*this).name(), reinterpret_cast<void*>(this), debug::MessageType::CopyConstructor)
		#define DEBUG_DESTRUCT_MESSAGE			debug::debugMessageForConstructionAndDestruction(typeid(*this).name(), reinterpret_cast<void*>(this), debug::MessageType::DefaultDestructor)
		#define DEBUG_MESSAGE(str)		std::cout << "[DEBUG]: "	<< str << std::endl;
		#define WARNING_MESSAGE(str)	std::cout << "[WARNING]: "	<< str << " : " << typeid(*this).name()+6 << std::endl;
	
	#else
	
		#define DEBUG_CONSTRUCT_MESSAGE
		#define DEBUG_COPY_CONSTRUCT_MESSAGE
		#define DEBUG_DESTRUCT_MESSAGE
		#define DEBUG_MESSAGE(str)
		#define WARNING_MESSAGE(str)
	
	#endif


}


/* ------------------------------------------------------------------------------------------------
	実体カウンタインターフェイスクラス
------------------------------------------------------------------------------------------------ */
template<class T>
class IInstanceCounter
{


	// ----------------------------------------------
	//	コンストラクタ / デストラクタ
	// ----------------------------------------------
protected:
	inline IInstanceCounter()										{ ++sNumberOfInstances; DEBUG_CONSTRUCT_MESSAGE; }
	inline IInstanceCounter(const IInstanceCounter& source)			{ ++sNumberOfInstances; DEBUG_COPY_CONSTRUCT_MESSAGE; }

public:
	inline virtual ~IInstanceCounter()								{ --sNumberOfInstances; DEBUG_DESTRUCT_MESSAGE; }
	

	// ----------------------------------------------
	//	静的メソッド
	// ----------------------------------------------
public:
	inline static uint32_t getNumberOfInstances() { return sNumberOfInstances; };


	// ----------------------------------------------
	//	静的変数
	// ----------------------------------------------
private:
	inline static uint32_t sNumberOfInstances = 0;


};


/* ------------------------------------------------------------------------------------------------
	インスタンスクラス
------------------------------------------------------------------------------------------------ */
class Instance final : public IInstanceCounter<Instance>
{


	// ----------------------------------------------
	//	コンストラクタ / デストラクタ
	// ----------------------------------------------
public:
	inline  Instance()							: IInstanceCounter<Instance>()			{ DEBUG_CONSTRUCT_MESSAGE; }
	inline	Instance(const Instance& source)	: IInstanceCounter<Instance>(source)	{ DEBUG_COPY_CONSTRUCT_MESSAGE; }
	inline	~Instance()	{ DEBUG_DESTRUCT_MESSAGE; }


	// ----------------------------------------------
	//	禁止
	// ----------------------------------------------
private:
	Instance& operator=(const Instance& source) = delete;


	// ----------------------------------------------
	//	変数
	// ----------------------------------------------
private:
	//	何かの変数	//


};


/* ------------------------------------------------------------------------------------------------
インスタンスクラス 2個目
------------------------------------------------------------------------------------------------ */
class Instance_2 final : public IInstanceCounter<Instance_2>
{


	// ----------------------------------------------
	//	コンストラクタ / デストラクタ
	// ----------------------------------------------
public:
	inline  Instance_2()							: IInstanceCounter<Instance_2>()		{ DEBUG_CONSTRUCT_MESSAGE; }
	inline	Instance_2(const Instance_2& source)	: IInstanceCounter<Instance_2>(source)	{ DEBUG_COPY_CONSTRUCT_MESSAGE; }
	inline	~Instance_2()																	{ DEBUG_DESTRUCT_MESSAGE; }


	// ----------------------------------------------
	//	禁止
	// ----------------------------------------------
private:
	Instance_2& operator=(const Instance_2& source) = delete;


	// ----------------------------------------------
	//	変数
	// ----------------------------------------------
private:
	//	何かの変数	//


};


/* ------------------------------------------------------------------------------------------------
	エントリーポイント
------------------------------------------------------------------------------------------------ */
int main()
{
	//	生ポ/ローカル変数の場合
	{
		Instance*	instance_ptr	= new Instance();
		Instance	instance_locale;

		Instance_2* instance_ptr_2	= new Instance_2();

		std::cout << Instance::getNumberOfInstances()	<< std::endl;
		std::cout << Instance_2::getNumberOfInstances() << std::endl;

		delete instance_ptr;
		delete instance_ptr_2;
	}

	DEBUG_MESSAGE("-------------------------------------------------------------------------------------------------------------");

	//	コンテナの場合
	{
		std::vector<Instance>	instances;
		std::vector<Instance_2> instances_2;

		instances.reserve(10);
		instances_2.reserve(10);

		instances.emplace_back();
		instances.emplace_back();
		instances.emplace_back();
		instances.emplace_back();
		instances.emplace_back();

		instances_2.emplace_back();
		instances_2.emplace_back();
		instances_2.emplace_back();

		std::cout << Instance::getNumberOfInstances() << std::endl;
		std::cout << Instance_2::getNumberOfInstances() << std::endl;
	}

	DEBUG_MESSAGE("-------------------------------------------------------------------------------------------------------------");

	//	混合
	{

		Instance*	instance_ptr = new Instance();
		Instance	instance_locale;
		std::vector<Instance>	instances;
		instances.reserve(10);

		instances.emplace_back();
		instances.emplace_back();
		instances.emplace_back();
		instances.emplace_back();
		instances.emplace_back();

		Instance_2* instance_ptr_2 = new Instance_2();
		std::vector<Instance_2> instances_2;
		instances_2.reserve(10);

		instances_2.emplace_back();
		instances_2.emplace_back();
		instances_2.emplace_back();

		std::cout << Instance::getNumberOfInstances() << std::endl;
		std::cout << Instance_2::getNumberOfInstances() << std::endl;

		delete instance_ptr;
		delete instance_ptr_2;
	}

	//	おわり
	getchar();

	return 0;
}
