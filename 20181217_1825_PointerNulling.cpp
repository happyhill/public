
/* ------------------------------------------------------------------------------------------------
	ヘッダ
------------------------------------------------------------------------------------------------ */
#include <iostream>
#include <functional>
#include <unordered_map>
#include <algorithm>

#include <string>


class Instance
{
public:
	using Ptr = std::unique_ptr<Instance>;

public:
	Instance()	{ std::cout << "New Instance has constructed. :" << std::hex << this << "\n"; }
	~Instance() { std::cout << "Instance has destructed.      :" << std::hex << this << "\n"; }
};

void nulling(Instance::Ptr& integer)
{
	delete integer.release();
}


/* ------------------------------------------------------------------------------------------------
	エントリーポイント
------------------------------------------------------------------------------------------------ */
int main()
{
	Instance::Ptr instance_ptr = std::make_unique<Instance>();

	std::cout << "instance_ptr : " << std::hex << instance_ptr.get() << "\n";

	nulling(instance_ptr);

	std::cout << "instance_ptr : " << std::hex << instance_ptr.get() << "\n";
	if (!instance_ptr)
	{
		std::cout << "instance_ptr is null.\n";
	}

	//	おわり
	getchar();

	return 0;
}