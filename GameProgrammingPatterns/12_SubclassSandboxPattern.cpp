﻿/* ==================================================================================================================================================
	
	Subclass Sandbox Pattern (from. Game Programming Patterns)
		
		remark:	基底クラスに実装した一連の操作を使用して、サブクラス内にビヘイビアを実装する。

================================================================================================================================================== */
/* ------------------------------------------------------------------------------------------------
	ヘッダ
------------------------------------------------------------------------------------------------ */
#include <typeinfo>
#include <iostream>
#include <functional>
#include <unordered_map>
#include <algorithm>

#include <string>


/* ------------------------------------------------------------------------------------------------
	debug名前空間
------------------------------------------------------------------------------------------------ */
namespace debug
{


	/* ------------------------------------------------------------------------------------------------
		定数
	------------------------------------------------------------------------------------------------ */
	constexpr uint32_t DEBUG_MESSAGE_ADDRESS_COLUMN = 50;


	/* ------------------------------------------------------------------------------------------------
		ユーティリティ関数
	------------------------------------------------------------------------------------------------ */
	// ----------------------------------------------
	//	コンストラクタ及びデストラクタで、それらが呼び出された事をコンソールに通知する
	// ----------------------------------------------
	void debugMessageForConstructionAndDestruction(const std::string& classname, void* address, bool isConstructor)
	{
		std::string str = (isConstructor) ? "[DEBUG]: New " + classname + " has constructed." :
			"[DEBUG]: " + classname + " has destructed.";

		for (; str.length() < DEBUG_MESSAGE_ADDRESS_COLUMN;)
		{
			str += " ";
		}

		std::cout << str << ": 0x" << std::hex << address << "\n";
	}


	/* ------------------------------------------------------------------------------------------------
		ヘルパマクロ
	------------------------------------------------------------------------------------------------ */
	#if defined(_DEBUG)
	
		#define DEBUG_CONSTRUCT_MESSAGE	debug::debugMessageForConstructionAndDestruction(typeid(*this).name(), reinterpret_cast<void*>(this), true)
		#define DEBUG_DESTRUCT_MESSAGE  debug::debugMessageForConstructionAndDestruction(typeid(*this).name(), reinterpret_cast<void*>(this), false)
	
	#else
	
		#define DEBUG_CONSTRUCT_MESSAGE
		#define DEBUG_DESTRUCT_MESSAGE
	
	#endif


}


/* ------------------------------------------------------------------------------------------------
	基底クラス
------------------------------------------------------------------------------------------------ */
class ISuperpower
{


	// ----------------------------------------------
	//	公開定義
	// ----------------------------------------------
public:
	using Ptr = std::unique_ptr<ISuperpower>;


	// ----------------------------------------------
	//	コンストラクタ / デストラクタ
	// ----------------------------------------------
protected:
	inline ISuperpower() { DEBUG_CONSTRUCT_MESSAGE; };

public:
	inline virtual ~ISuperpower() { DEBUG_DESTRUCT_MESSAGE; };


	// ----------------------------------------------
	//	純粋仮想関数
	// ----------------------------------------------
public:
	virtual void activate() = 0;


	// ----------------------------------------------
	//	継承利用関数
	// ----------------------------------------------
	//		remark: 派生先のサンドボックスクラスで利用するユーティリティ関数群はここに定義する。
	//		note:	Game Programming Patternsでは、protected/non-virtualな関数で宣言することで、サブクラスで利用する関数として定義すると記述があるが、
	//				その方法ではサンドボックスクラスでのoverrideを禁止できない問題がある。
	//				ここでは、protected/finalで定義する方がより厳密となるためそのように変更している。
protected:
	virtual void say(std::string&& message) final { std::cout << message << std::endl; };
	virtual void line() final { std::cout << "-------------------------------------------------------\n"; }


};


/* ------------------------------------------------------------------------------------------------
	派生クラス
------------------------------------------------------------------------------------------------ */
class SandBox final : public ISuperpower
{


	// ----------------------------------------------
	//	コンストラクタ / デストラクタ
	// ----------------------------------------------
public:
	inline explicit SandBox(std::string&& name) : ISuperpower(), mName(name) { DEBUG_CONSTRUCT_MESSAGE; };
	inline virtual ~SandBox() { DEBUG_DESTRUCT_MESSAGE; };


	// ----------------------------------------------
	//	オーバーライド関数
	// ----------------------------------------------
public:
	virtual void activate();


private:
	std::string mName;

};


// ----------------------------------------------
//	オーバーライド関数
// ----------------------------------------------
void SandBox::activate()
{
	line();
	say(mName + " : Sandbox class activated!!");
	line();
}


/* ------------------------------------------------------------------------------------------------
	エントリーポイント
------------------------------------------------------------------------------------------------ */
int main()
{

	{
		ISuperpower::Ptr superpower = std::make_unique<SandBox>("Hoge");
		superpower->activate();
	}

	{
		std::vector<ISuperpower::Ptr> superpowers;
		superpowers.emplace_back(std::make_unique<SandBox>("Hoge"));
		superpowers.emplace_back(std::make_unique<SandBox>("Fuga"));
		superpowers.emplace_back(std::make_unique<SandBox>("Piyo"));

		for (auto& superpower : superpowers)
		{
			superpower->activate();
		}
	}

	//	おわり
	getchar();

	return 0;
}
