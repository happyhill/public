/* ==================================================================================================================================================
	
	Type Object Pattern (from. Game Programming Patterns)
		
		remark:	クラスを1つ作成し、そのクラスのインスタンス1つ1つが異なった型を表すようにする。
				これで多くの「クラス」を柔軟に作成できるようになる。

================================================================================================================================================== */
/* ------------------------------------------------------------------------------------------------
	ヘッダ
------------------------------------------------------------------------------------------------ */
#include <typeinfo>
#include <iostream>
#include <functional>
#include <unordered_map>
#include <algorithm>

#include <memory>
#include <string>

#include <locale>


/* ------------------------------------------------------------------------------------------------
	debug名前空間
------------------------------------------------------------------------------------------------ */
namespace debug
{


	/* ------------------------------------------------------------------------------------------------
		定数
	------------------------------------------------------------------------------------------------ */
	constexpr uint32_t DEBUG_MESSAGE_ADDRESS_COLUMN = 95;

	enum class MessageType : uint8_t
	{
		DefaultConstructor	= 0,
		CopyConstructor		= 1,
		DefaultDestructor	= 2,
	};


	/* ------------------------------------------------------------------------------------------------
		ユーティリティ関数
	------------------------------------------------------------------------------------------------ */
	// ----------------------------------------------
	//	コンストラクタ及びデストラクタで、それらが呼び出された事をコンソールに通知する
	// ----------------------------------------------
	void debugMessageForConstructionAndDestruction(const std::string& classname, void* address, MessageType messageType)
	{

		switch (messageType)
		{
			case MessageType::DefaultConstructor:
				break;
			case MessageType::CopyConstructor:
				break;
			case MessageType::DefaultDestructor:
				break;
			default:
				break;
		}

		std::string str;
		switch (messageType)
		{
			case MessageType::DefaultConstructor:
				str = "[DEBUG]: New " + classname + " has constructed.";
				break;
			case MessageType::CopyConstructor:
				str = "[DEBUG]: New " + classname + " has constructed, from Copy constructor.";
				break;
			case MessageType::DefaultDestructor:
				str = "[DEBUG]: " + classname + " has destructed.";
				break;
			default:
				break;
		}

		for (; str.length() < DEBUG_MESSAGE_ADDRESS_COLUMN;)
		{
			str += " ";
		}

		std::cout << str << ": 0x" << std::hex << address << std::dec << "\n";
	}


	/* ------------------------------------------------------------------------------------------------
		ヘルパマクロ
	------------------------------------------------------------------------------------------------ */
	#if defined(_DEBUG)
	
		#define DEBUG_CONSTRUCT_MESSAGE			debug::debugMessageForConstructionAndDestruction(typeid(*this).name(), reinterpret_cast<void*>(this), debug::MessageType::DefaultConstructor)
		#define DEBUG_COPY_CONSTRUCT_MESSAGE	debug::debugMessageForConstructionAndDestruction(typeid(*this).name(), reinterpret_cast<void*>(this), debug::MessageType::CopyConstructor)
		#define DEBUG_DESTRUCT_MESSAGE			debug::debugMessageForConstructionAndDestruction(typeid(*this).name(), reinterpret_cast<void*>(this), debug::MessageType::DefaultDestructor)
		#define DEBUG_MESSAGE(str)		std::cout << "[DEBUG]: "	<< str << std::endl;
		#define WARNING_MESSAGE(str)	std::cout << "[WARNING]: "	<< str << " : " << typeid(*this).name()+6 << std::endl;
	
	#else
	
		#define DEBUG_CONSTRUCT_MESSAGE
		#define DEBUG_COPY_CONSTRUCT_MESSAGE
		#define DEBUG_DESTRUCT_MESSAGE
		#define DEBUG_MESSAGE(str)
		#define WARNING_MESSAGE(str)
	
	#endif


}


/* ------------------------------------------------------------------------------------------------
	系統クラス(TypeObject)
------------------------------------------------------------------------------------------------ */
class Monster;	//	createMonster関数のための前方宣言
class Breed final
{


	// ----------------------------------------------
	//	コンストラクタ / デストラクタ
	// ----------------------------------------------
public:
	inline Breed();// : mInitialHealth(0u), mAttackMessage("Not available message.") { DEBUG_CONSTRUCT_MESSAGE; }
	inline explicit Breed(const Breed* parent, uint32_t initialHealth = 0u, const std::string& attackMessage = std::string());
	inline ~Breed()								{ DEBUG_DESTRUCT_MESSAGE; }
	

	// ----------------------------------------------
	//	アクセサ
	// ----------------------------------------------
public:
	inline uint32_t				getInitialHealth()	const { return mInitialHealth; }
	inline const std::string&	getAttackMessage()	const { return mAttackMessage; }


	// ----------------------------------------------
	//	公開関数
	// ----------------------------------------------
public:
	inline std::unique_ptr<Monster> createMonster();


	// ----------------------------------------------
	//	禁止
	// ----------------------------------------------
private:
	inline explicit Breed(const Breed& source)		= delete;
	inline Breed& operator=(const Breed& source)	= delete;


	// ----------------------------------------------
	//	変数
	// ----------------------------------------------
private:
	uint32_t	mInitialHealth;
	std::string	mAttackMessage;


};

// ----------------------------------------------
//	コンストラクタ / デストラクタ
// ----------------------------------------------
inline Breed::Breed() :
	mInitialHealth(0u),
	mAttackMessage("Not available message.")
{
	DEBUG_CONSTRUCT_MESSAGE;
}

inline Breed::Breed(const Breed* parent, uint32_t initialHealth, const std::string& attackMessage) :
	mInitialHealth(initialHealth), 
	mAttackMessage(attackMessage) 
{ 
	//	継承ルール
	{
		if (parent != nullptr)
		{
			if (initialHealth == 0u)
			{
				mInitialHealth = parent->getInitialHealth();
			}

			if (attackMessage.empty())
			{
				mAttackMessage = parent->getAttackMessage();
			}
		}
	}

	DEBUG_CONSTRUCT_MESSAGE; 
}


/* ------------------------------------------------------------------------------------------------
	モンスタークラス(InstanceObject)
------------------------------------------------------------------------------------------------ */
class Monster final
{


	// ----------------------------------------------
	//	別名定義
	// ----------------------------------------------
public:
	using Ptr = std::unique_ptr<Monster>;


	// ----------------------------------------------
	//	コンストラクタ / デストラクタ
	// ----------------------------------------------
public:
	inline explicit Monster(const Breed& breed) : mHealth(breed.getInitialHealth()), mBreed(breed)	{ DEBUG_CONSTRUCT_MESSAGE; }
	inline	~Monster()																				{ DEBUG_DESTRUCT_MESSAGE; }


	// ----------------------------------------------
	//	公開関数
	// ----------------------------------------------
public:
	inline void attack()		const { std::cout << mBreed.getAttackMessage() << std::endl; }
	inline void printHealth()	const { std::cout << "HP:" << mHealth << std::endl; }


	// ----------------------------------------------
	//	禁止
	// ----------------------------------------------
private:
	inline explicit Monster(const Monster& source)		= delete;
	inline Monster& operator=(const Monster& source)	= delete;


	// ----------------------------------------------
	//	変数
	// ----------------------------------------------
private:
	uint32_t		mHealth;
	const Breed&	mBreed;


};

inline Monster::Ptr Breed::createMonster()
{
	return std::make_unique<Monster>(*this);
}


/* ------------------------------------------------------------------------------------------------
	外部データ構造体
------------------------------------------------------------------------------------------------ */
struct ExternalData final
{
	inline ExternalData(std::string&& _breedName, std::string&& _parentBreedName, uint32_t _initialHealth, std::string&& _attackMessage) : 
		breedName(std::move(_breedName)), 
		parentBreedName(std::move(_parentBreedName)),
		initialHealth(_initialHealth), 
		attackMessage(std::move(_attackMessage))
	{};

	std::string	breedName;
	std::string	parentBreedName;
	uint32_t	initialHealth;
	std::string	attackMessage;
};


// ----------------------------------------------
//	外部データ
//		remark:	実際はJSON等のシリアライズデータから読み込む
// ----------------------------------------------
ExternalData breedData[]
{
	{ "Troll",			std::string(),	25, "巨人が攻撃した！"},
	{ "Troll Archer",	"Troll",		0,	"巨人の射手が矢を放った！"},
	{ "Troll Wizard",	"Troll",		20,	"巨人の魔法使いが呪文を唱えた！"},
};


/* ------------------------------------------------------------------------------------------------
	エントリーポイント
------------------------------------------------------------------------------------------------ */
int main()
{
	{
		std::unordered_map<std::string, Breed> breeds;
		breeds.reserve(3);

		//	外部データを読み込み
		{
			for (auto& itr : breedData)
			{
				const Breed* parentBreed = (breeds.count(itr.parentBreedName)) ? &breeds[itr.parentBreedName] : nullptr;

				breeds.try_emplace(itr.breedName, parentBreed, itr.initialHealth, itr.attackMessage);
			}
		}

		std::vector<Monster::Ptr> monsters;

		monsters.push_back(breeds["Troll"].createMonster());
		monsters.push_back(breeds["Troll Archer"].createMonster());
		monsters.push_back(breeds["Troll Wizard"].createMonster());

		for (auto& itr : monsters)
		{
			itr->attack();
			itr->printHealth();
		}
	}


	//	おわり
	getchar();

	return 0;
}

