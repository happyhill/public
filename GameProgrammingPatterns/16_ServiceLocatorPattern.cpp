﻿/* ==================================================================================================================================================
	
	Service Locator Pattern (from. Game Programming Patterns)
		
		remark:	サービスを利用するコードに対して、サービスが実装された具象クラスと結合することなしに、サービスへのグローバルなアクセスポイントを提供する。

================================================================================================================================================== */
/* ------------------------------------------------------------------------------------------------
	ヘッダ
------------------------------------------------------------------------------------------------ */
#include <typeinfo>
#include <iostream>
#include <functional>
#include <unordered_map>
#include <algorithm>

#include <memory>
#include <string>


/* ------------------------------------------------------------------------------------------------
	debug名前空間
------------------------------------------------------------------------------------------------ */
namespace debug
{


	/* ------------------------------------------------------------------------------------------------
		定数
	------------------------------------------------------------------------------------------------ */
	constexpr uint32_t DEBUG_MESSAGE_ADDRESS_COLUMN = 50;


	/* ------------------------------------------------------------------------------------------------
		ユーティリティ関数
	------------------------------------------------------------------------------------------------ */
	// ----------------------------------------------
	//	コンストラクタ及びデストラクタで、それらが呼び出された事をコンソールに通知する
	// ----------------------------------------------
	void debugMessageForConstructionAndDestruction(const std::string& classname, void* address, bool isConstructor)
	{
		std::string str = (isConstructor) ? "[DEBUG]: New " + classname + " has constructed." :
			"[DEBUG]: " + classname + " has destructed.";

		for (; str.length() < DEBUG_MESSAGE_ADDRESS_COLUMN;)
		{
			str += " ";
		}

		std::cout << str << ": 0x" << std::hex << address << "\n";
	}


	/* ------------------------------------------------------------------------------------------------
		ヘルパマクロ
	------------------------------------------------------------------------------------------------ */
	#if defined(_DEBUG)
	
		#define DEBUG_CONSTRUCT_MESSAGE	debug::debugMessageForConstructionAndDestruction(typeid(*this).name(), reinterpret_cast<void*>(this), true)
		#define DEBUG_DESTRUCT_MESSAGE  debug::debugMessageForConstructionAndDestruction(typeid(*this).name(), reinterpret_cast<void*>(this), false)
		#define DEBUG_MESSAGE(str)		std::cout << "[DEBUG]: "	<< str << std::endl;
		#define WARNING_MESSAGE(str)	std::cout << "[WARNING]: "	<< str << " : " << typeid(*this).name()+6 << std::endl;
	
	#else
	
		#define DEBUG_CONSTRUCT_MESSAGE
		#define DEBUG_DESTRUCT_MESSAGE
		#define DEBUG_MESSAGE(str)
		#define WARNING_MESSAGE(str)
	
	#endif


}


/* ------------------------------------------------------------------------------------------------
	APIクラス
------------------------------------------------------------------------------------------------ */
class SoundAPI
{


	// ----------------------------------------------
	//	公開定義
	// ----------------------------------------------
public:
	using Label		= std::string;
	using SoundData = std::string;	//	実際はMIDIデータや音声波形データのLBO


	// ----------------------------------------------
	//	コンストラクタ / デストラクタ
	// ----------------------------------------------
public:
	SoundAPI() : mSounds()	{ DEBUG_CONSTRUCT_MESSAGE; }
	~SoundAPI()				{ DEBUG_DESTRUCT_MESSAGE; }


	// ----------------------------------------------
	//	公開関数
	// ----------------------------------------------
public:
	void PlaySound(Label&& soundLabel);
	void StopSound(Label&& soundLabel);
	void LoadSound(Label&& soundLabel, const SoundData& soundData);


	// ----------------------------------------------
	//	変数
	// ----------------------------------------------
private:
	std::unordered_map<Label, SoundData> mSounds;


};


// ----------------------------------------------
//	公開関数
// ----------------------------------------------
void SoundAPI::PlaySound(Label&& soundLabel)
{
	if (mSounds.count(soundLabel) != 0)
	{
		std::cout << "Play sound [" << soundLabel << "]: ♪ " << mSounds[soundLabel] << std::endl;
	}
}

void SoundAPI::StopSound(Label&& soundLabel)
{
	if (mSounds.count(soundLabel) != 0)
	{
		std::cout << "Stop sound [" << soundLabel << "]: " << std::endl;
	}
}

void SoundAPI::LoadSound(Label&& soundLabel, const SoundData& soundData)
{
	if (mSounds.count(soundLabel) == 0)
	{
		mSounds.insert(std::pair{ soundLabel, soundData });
	}
}


/* ------------------------------------------------------------------------------------------------
	サービスインターフェイスクラス
------------------------------------------------------------------------------------------------ */
class IAudio
{


	// ----------------------------------------------
	//	公開定義
	// ----------------------------------------------
public:
	using Ptr = std::shared_ptr<IAudio>;


	// ----------------------------------------------
	//	コンストラクタ / デストラクタ
	// ----------------------------------------------
protected:
	inline IAudio() { DEBUG_CONSTRUCT_MESSAGE; }

public:
	inline virtual ~IAudio() { DEBUG_DESTRUCT_MESSAGE; }


	// ----------------------------------------------
	//	純粋仮想関数
	// ----------------------------------------------
public:
	virtual void playSound(SoundAPI::Label&& soundLabel)	= 0;
	virtual void stopSound(SoundAPI::Label&& soundLabel)	= 0;
	virtual void loadSound(SoundAPI::Label&& soundLabel, const SoundAPI::SoundData& soundData) = 0;


};


/* ------------------------------------------------------------------------------------------------
	サービスプロバイダクラス
------------------------------------------------------------------------------------------------ */
class ConsoleAudio final : public IAudio
{


	// ----------------------------------------------
	//	コンストラクタ / デストラクタ
	// ----------------------------------------------
public:
	inline ConsoleAudio() : IAudio(), mSoundAPI()	{ DEBUG_CONSTRUCT_MESSAGE; }
	inline ~ConsoleAudio()							{ DEBUG_DESTRUCT_MESSAGE; }


	// ----------------------------------------------
	//	オーバーライド関数
	// ----------------------------------------------
public:
	virtual void playSound(SoundAPI::Label&& soundLabel)	override;
	virtual void stopSound(SoundAPI::Label&& soundLabel)	override;
	virtual void loadSound(SoundAPI::Label&& soundLabel, const SoundAPI::SoundData& soundData) override;


	// ----------------------------------------------
	//	変数
	// ----------------------------------------------
private:
	SoundAPI mSoundAPI;


};


// ----------------------------------------------
//	オーバーライド関数
// ----------------------------------------------
void ConsoleAudio::playSound(SoundAPI::Label&& soundLabel)
{
	mSoundAPI.PlaySound(std::forward<SoundAPI::Label>(soundLabel));
}

void ConsoleAudio::stopSound(SoundAPI::Label&& soundLabel)
{
	mSoundAPI.StopSound(std::forward<SoundAPI::Label>(soundLabel));
}

void ConsoleAudio::loadSound(SoundAPI::Label&& soundLabel, const SoundAPI::SoundData& soundData)
{
	mSoundAPI.LoadSound(std::forward<SoundAPI::Label>(soundLabel), soundData);
}


/* ------------------------------------------------------------------------------------------------
	NULLオブジェクトサービスクラス
------------------------------------------------------------------------------------------------ */
class NullAudio final : public IAudio
{


	// ----------------------------------------------
	//	コンストラクタ / デストラクタ
	// ----------------------------------------------
public:
	inline NullAudio()  { DEBUG_CONSTRUCT_MESSAGE; }
	inline ~NullAudio() { DEBUG_DESTRUCT_MESSAGE; }


	// ----------------------------------------------
	//	オーバーライド関数
	// ----------------------------------------------
public:
	virtual void playSound(SoundAPI::Label&& soundLabel)	override;
	virtual void stopSound(SoundAPI::Label&& soundLabel)	override;
	virtual void loadSound(SoundAPI::Label&& soundLabel, const SoundAPI::SoundData& soundData) override;

};


// ----------------------------------------------
//	オーバーライド関数
// ----------------------------------------------
void NullAudio::playSound(SoundAPI::Label&& soundLabel)
{
	WARNING_MESSAGE("Null service object referenced.");
}

void NullAudio::stopSound(SoundAPI::Label&& soundLabel)
{
	WARNING_MESSAGE("Null service object referenced.");
}

void NullAudio::loadSound(SoundAPI::Label&& soundLabel, const SoundAPI::SoundData& soundData)
{
	WARNING_MESSAGE("Null service object referenced.");
}


/* ------------------------------------------------------------------------------------------------
	ロケータクラス
------------------------------------------------------------------------------------------------ */
class Locator final
{


	// ----------------------------------------------
	//	コンストラクタ / デストラクタ
	// ----------------------------------------------
public:
	inline Locator()	{ DEBUG_CONSTRUCT_MESSAGE; }
	inline ~Locator()	{ DEBUG_DESTRUCT_MESSAGE; }


	// ----------------------------------------------
	//	公開関数
	// ----------------------------------------------
public:
	static IAudio::Ptr getAudio()				{ return mAudioService; }
	static void provide(IAudio::Ptr& service)	{ mAudioService = (service) ? service : mNullAudioService; }


	// ----------------------------------------------
	//	変数
	// ----------------------------------------------
private:
	static IAudio::Ptr mAudioService;
	static IAudio::Ptr mNullAudioService;


};


// ----------------------------------------------
//	静的メンバ変数の外部定義
// ----------------------------------------------
IAudio::Ptr Locator::mAudioService		= std::make_shared<NullAudio>();
IAudio::Ptr Locator::mNullAudioService	= std::make_shared<NullAudio>();


/* ------------------------------------------------------------------------------------------------
	エントリーポイント
------------------------------------------------------------------------------------------------ */
int main()
{

	Locator::getAudio()->loadSound("Null", "NullNullNull.");

	{
		IAudio::Ptr audio(new ConsoleAudio());
		Locator::provide(audio);
	}

	{
		Locator::getAudio()->loadSound("Hoge",	"HogeHogeHoge.");
		Locator::getAudio()->loadSound("Piyo",	"PiyoPiyoPiyo.");
		Locator::getAudio()->loadSound("Fuga",	"FugaFugaFuga.");
		Locator::getAudio()->loadSound("All",	"HogePiyoFuga.");
	}

	{
		Locator::getAudio()->playSound("Hoge");
		Locator::getAudio()->playSound("Piyo");
		Locator::getAudio()->playSound("Fuga");
		Locator::getAudio()->playSound("All");
	}

	{
		Locator::getAudio()->stopSound("Hoge");
		Locator::getAudio()->stopSound("Piyo");
		Locator::getAudio()->stopSound("Fuga");
		Locator::getAudio()->stopSound("All");
	}

	//	おわり
	getchar();

	return 0;
}
