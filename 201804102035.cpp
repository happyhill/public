﻿// Headers
#include <iostream>
#include <random>
#include <bitset>
#include <memory>
#include <cstdint>


int main()
{
	//	initialize random_device
	std::random_device hardware_entropy_resource_generator;
	auto get_hardware_entropy_resource_64bit = [&hardware_entropy_resource_generator]() 
	{
		return ((uint64_t)hardware_entropy_resource_generator() << 32u) | hardware_entropy_resource_generator(); 
	};

	//	get seed
	const uint64_t initial_seed = get_hardware_entropy_resource_64bit();
	std::cout << "initial_seed : " << (std::bitset<64>)initial_seed << std::endl;

	//	initialize mt64
	std::mt19937_64 random_number_generator(initial_seed);


#if 1

	//	constant numbers
	constexpr uint8_t	NUMBER_OF_ALPHABETS		= 26u;
	constexpr uint32_t	NUMBER_OF_TRIALS		= 1u << 20u;
	constexpr uint32_t	NUMBER_OF_TRIALS_DIV10	= NUMBER_OF_TRIALS / 10u;

	//	initialize result buffers
	std::unique_ptr<uint32_t[]> count_of_trials_buffer_AA = std::make_unique<uint32_t[]>(NUMBER_OF_TRIALS);
	std::unique_ptr<uint32_t[]> count_of_trials_buffer_AB = std::make_unique<uint32_t[]>(NUMBER_OF_TRIALS);

	//	define counting function
	auto counting = [&](const char _old, const char _new, uint32_t* const result_buffer)
	{
		for (uint32_t i = 0u; i < NUMBER_OF_TRIALS; ++i)
		{
			//	initialize old number buffer
			char old = INT8_MAX;

			//	initialize counter
			uint32_t count_of_keyings = 0u;

			while (true)
			{
				++count_of_keyings;
				char now = random_number_generator() % NUMBER_OF_ALPHABETS;

				//	AA
				if (old == _old && now == _new)
				{
					result_buffer[i] = count_of_keyings;
					break;
				}

				//	set old number
				old = std::move(now);
			}

			//	output progress
			if (i % NUMBER_OF_TRIALS_DIV10 == 0u)
			{
				std::system("cls");
				std::cout << "initial_seed : " << (std::bitset<64u>)initial_seed << '\n';
				std::cout << ((_new) ? "[ A->B ]\n" : "[ A->A ]\n");

				std::cout << "progress : [";
				for (uint32_t j = 0u; j < (i / NUMBER_OF_TRIALS_DIV10); ++j)
				{
					std::cout << '*';
				}
				for (uint32_t j = 0u; j < (10 - (i / NUMBER_OF_TRIALS_DIV10)); ++j)
				{
					std::cout << '_';
				}
				std::cout << "]\n";
			}
		}
	};

	//	define averaging and console output function
	auto averaging = [&](const uint32_t* const result_buffer)
	{
		uint64_t sum_keyings = 0u;
		for (uint32_t i = 0u; i<NUMBER_OF_TRIALS; ++i)
		{
			sum_keyings += result_buffer[i];
		}
		std::cout << "average number of keyings : " << sum_keyings / NUMBER_OF_TRIALS << '\n';
	};

	//	run!
	counting(0, 0, count_of_trials_buffer_AA.get());
	counting(0, 1, count_of_trials_buffer_AB.get());

	//	output results!
	std::cout << std::flush;
	std::cout << "number of trials : " << NUMBER_OF_TRIALS << "\n\n";

	std::cout << "[ A->A ]\n";
	averaging(count_of_trials_buffer_AA.get());

	std::cout << "[ A->B ]\n";
	averaging(count_of_trials_buffer_AB.get());

	std::cout << std::flush;


#else

	//	constant numbers
	constexpr uint8_t	NUMBER_OF_ALPHABETS		= 26u;
	constexpr uint32_t	NUMBER_OF_KEYINGS		= INT32_MAX;
	constexpr uint32_t	NUMBER_OF_KEYINGS_DIV10	= NUMBER_OF_KEYINGS / 10u;

	//	initialize old number buffer
	char old = INT8_MAX;

	//	initialize counter
	uint32_t count_of_AA = 0u;
	uint32_t count_of_AB = 0u;

	//	run!
	for (uint32_t i = 0u; i < NUMBER_OF_KEYINGS; ++i)
	{
		char now = random_number_generator() % NUMBER_OF_ALPHABETS;

		// A_
		if (old == 0)
		{
			//	AA
			if (now == 0)
			{
				++count_of_AA;

				//	reset
				now = INT8_MAX;
				random_number_generator.seed(get_hardware_entropy_resource_64bit());
			}

			//	AB
			if (now == 1)
			{
				++count_of_AB;

				//	reset
				now = INT8_MAX;
				random_number_generator.seed(get_hardware_entropy_resource_64bit());
			}
		}

		//	output progress
		if (i % NUMBER_OF_KEYINGS_DIV10 == 0u)
		{
			std::system("cls");
			std::cout << "initial_seed : " << (std::bitset<64u>)initial_seed << '\n' << "progress : [";
			for (uint32_t j = 0u; j < (i / NUMBER_OF_KEYINGS_DIV10); ++j)
			{
				std::cout << '*';
			}
			for (uint32_t j = 0u; j < (10 - (i / NUMBER_OF_KEYINGS_DIV10)); ++j)
			{
				std::cout << '_';
			}
			std::cout << "]\n";
		}

		//	set old number
		old = std::move(now);
	}

	//	output results!
	std::cout << std::flush;
	std::cout << "number of keyings : " << NUMBER_OF_KEYINGS << "\n\n";

	std::cout << "[ A->A ]\n";
	std::cout << "number of occurrences : " << count_of_AA << '\n';
	std::cout << "frequency : " << ((double)count_of_AA / (double)NUMBER_OF_KEYINGS)*100.0 << "%\n";
	std::cout << "[ A->B ]\n";
	std::cout << "number of occurrences : " << count_of_AB << '\n';
	std::cout << "frequency : " << ((double)count_of_AB / (double)NUMBER_OF_KEYINGS)*100.0 << "%\n";

	std::cout << std::flush;


#endif

	//	waiting
	getchar();

	//	done!
	return 0;

}